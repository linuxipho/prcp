from pathlib import Path

import frappe
import pandas as pd


def run():
    """Chargement des fiches des membres"""

    file = 'data/BDD_2022.xlsx'
    path = Path(__file__).parent / file
    df = pd.read_excel(path, sheet_name='FICHES', dtype=str, na_filter=False, index_col=None, engine='openpyxl', skiprows=3)

    # print(df.columns)

    df["Cause du chômage"].replace({
        "nc": 'NC',
        'licenciement': 'Licenciement'
    }, inplace=True)

    df["Publicité"].replace({
        "(non définie ou non communiquée)": 'NC'
    }, inplace=True)

    df["Civilité"].replace({
        'f': 'F',
        'm': 'M'
    }, inplace=True)

    df["DELD (oui/non)"].replace({
        'oui': 'Oui'
    }, inplace=True)

    df["Véhicule"].replace({
        'oui': 'Oui',
        'non': 'Non'
    }, inplace=True)

    df["Logement"].replace({
        'non': 'Non'
    }, inplace=True)

    df["Sortie positive"].replace({
        'NC': ''
    }, inplace=True)

    df['Pôle Emploi'].replace({
        "LA PLAINE": "La Plaine",
        "St- Michel": "Saint-Michel",
        "Toulouse St-michel": "Saint-Michel",
        "Occitane": "Toulouse Occitane",
        "St Georges": "Saint-Georges"
    }, inplace=True)

    df["Secteur d'activité"].replace({
        "Agen": "Agent",
        # "Agent d'accueil / Secretariat / Comptabilité": "",
        # "Agent d'entretien": None,
        "agent d'entretien": "Agent d'entretien",
        "Agent d'entretien  / Aide à domicile": "Agent d'entretien / Aide à domicile",
        # "Agent d'entretien / Aide à domicile": None,
        # "Association": "",
        # "Automobile": "",
        "autres": "Autre",
        "Banque/Assurance/Immobilier": "Banque / Assurance / Immobilier",
        # "BTP": "",
        # "Chef de projet": "",
        # "CIP": "",
        # "Commerce": "",
        # "Couture": "",
        # "Crèche": "",
        # "Créer son entreprise": "",
        # "Distribution Chronopost": "",
        # "Education": "",
        "Enseignante": "Education",
        "Gde distribution": "Grande distribution",
        "Gde distribution / vente": "Grande distribution",
        "Gde distribution / Vente": "Grande distribution",
        "Gde distribution/Vente": "Grande distribution",
        "Hotellerie / Restauration": "Hôtellerie / Restauration",
        # "Hôtellerie / Restauration": "",
        "Hotellerie/Restauration": "Hôtellerie / Restauration",
        # "Industrie": "",
        # "Informatique": "",
        "Ingeniorat": "Ingénierie",
        # "Jardinage": "",
        "Manutention/Cariste": "Manutention / Cariste",
        "Manutentionnaire/Cariste": "Manutention / Cariste",
        # "Paramédical / Médical": "",
        # "Parascolaire / Scolaire": "",
        # "Ressources Humaines": "",
        "Restauration": "Hôtellerie / Restauration",
        # "Sécurité": "",
        # "Social": "",
        # "Tourisme": "",
        # "Transport": ""
    }, inplace=True)

    df["Téléphone 1"].replace({
        '\\s+': ' '
    }, inplace=True, regex=True)

    df["Téléphone 2"].replace({
        '\\s+': ' '
    }, inplace=True, regex=True)

    df["Commentaire"] = df["Commentaire"].str.strip()
    df["Adresse électronique"] = df["Adresse électronique"].str.replace(',', '.')
    df["Adresse électronique"] = df["Adresse électronique"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

    # records = df.where(pd.notnull(df), None, inplace=True)

    c = ['Unnamed: 0', 'x', 'Réf.', 'Nom', 'Prénom', 'Date arrivée', 'Cotisation', 'Nom de JF', 'Naissance', 'Age',
         'Civilité', 'Adresse', 'Code QP', 'Code P.', 'Commune', 'Téléphone 1', 'Téléphone 2', 'Adresse électronique',
         'Logement', 'Véhicule', 'Publicité', 'Principal objectif recherché', 'Cause du chômage', 'Situation',
         'Référent (RSA)', 'Niveau de formation', 'Pôle Emploi', 'DELD (oui/non)', 'Emploi: profession ou projet',
         "Secteur d'activité", 'Sortie positive', 'Commentaire', 'Unnamed: 32', 'Drapeau D1', 'Drapeau D3',
         'D1 et D3', 'Unnamed: 36', 'Déclassement?']

    frappe.flags.in_import = True

    try:

        for idx, row in df.iterrows():

            if not row['Réf.'] or not row['Nom']:
                continue

            ref = f"{int(row['Réf.']):0>5d}"

            if frappe.db.exists(dict(doctype='Participant', ref=ref)):
                nom_usuel = frappe.get_value('Participant', ref, 'nom_usuel')
                if row['Nom'] == nom_usuel:
                    print(f"Le participant {ref} existe déjà")
                    continue
                else:
                    print(f'Nouvel adhérent -> réf.{ref}')

            doc = frappe.get_doc(
                doctype='Participant',
                ref=ref,
                genre=row['Civilité'],
                actif=True,
                prenom=row['Prénom'] or None,
                nom_usuel=row['Nom'] or None,
                nom_naissance=row['Nom de JF'] or None,
                date_naissance=row['Naissance'][:10] if not pd.isna(row['Naissance']) else None,
                civilite=row['Civilité'] or None,
                adresse=row['Adresse'] or None,
                code_postal=row['Code P.'] or None,
                commune=row['Commune'] or None,
                tel=format_phone(row),
                mel=valid_email(row['Adresse électronique']),
                arrivee=frappe.utils.get_datetime(row['Date arrivée']).year,
                publicite=row['Publicité'] or None,
            )

            doc.insert()
            if row['Commentaire']:
                doc.add_comment('Comment', text=row['Commentaire'])

            if row['Situation'] not in ['Bénévolat', 'Bienfaiteur', 'Partenaire']:

                emploi = row.get('Emploi: profession ou projet', None)
                if row['Emploi: profession ou projet']:
                    if not frappe.db.exists('Emploi', row['Emploi: profession ou projet']):
                        frappe.get_doc(
                            doctype='Emploi',
                            label=row['Emploi: profession ou projet']
                        ).insert()

                fiche = frappe.get_doc(
                    doctype='Accueil adherent',
                    participant=doc.name,
                    nom_participant=doc.get_value('title'),
                    date_naissance=doc.get_value('date_naissance'),
                    age=doc.get_value('age'),
                    date_accueil=row['Date arrivée'][:10] if not pd.isna(row['Date arrivée']) else None,
                    objectif=row['Principal objectif recherché'] or None,
                    logement=row['Logement'] or None,
                    vehicule=row['Véhicule'] or None,
                    chomage=row['Cause du chômage'] or None,
                    referent_rsa=row['Référent (RSA)'] or None,
                    niveau_formation=row['Niveau de formation'] or None,
                    pole_emploi=row['Pôle Emploi'] or None,
                    deld=row['DELD (oui/non)'] or None,
                    emploi=emploi,
                    secteur_activite=row["Secteur d'activité"] or None,
                    sortie_positive=row['Sortie positive'] or None,
                    situation=row['Situation'] or None
                )
                fiche.insert()

    finally:
        frappe.flags.in_import = False


def format_phone(r):
    raw = r['Téléphone 2'] or r['Téléphone 1'] or None
    return raw.strip() if raw else raw  # '+33-' + raw.replace(' ', '')[1:] if raw else raw


def valid_email(mail):

    if mail and frappe.utils.validate_email_address(mail):
        return mail
    else:
        return None


def cahier_liaison():
    file = 'data/BDD_2022.xlsx'
    path = Path(__file__).parent / file
    df = pd.read_excel(path, sheet_name='Cahier', dtype=str, na_filter=False, index_col=None, engine='openpyxl', skiprows=3)

    df["Motif de visite"].replace({
        'nc': 'NC',
        'partenariat ou bienfaiteur': 'Partenariat ou bienfaiteur'
    }, inplace=True)

    for idx, row in df.iterrows():
        if not row['Réf.']:
            break
        ref = f"{int(row['Réf.']):0>5d}"
        print(ref)
        participant = frappe.db.get_value('Participant', filters={'ref': ref})
        doc = frappe.get_doc(
            doctype='Entree cahier liaison',
            participant=participant,
            date=row['Date'],
            motif_de_visite=row['Motif de visite'],
            commentaire=row['Commentaire'] or None
        )
        doc.insert()


if __name__ == '__main__':
    run()
