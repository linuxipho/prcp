import frappe

LOG = frappe.logger("prcp.utils", allow_site=True, file_count=10)


@frappe.whitelist()
def compute_participant_age():
    """Met à jour l'âge des participants"""

    frappe.db.sql("""
    UPDATE `tabParticipant` SET age = COALESCE(TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()), 0)
    """)


def daily():
    LOG.debug('=== Execute daily tasks ===')
    compute_participant_age()
