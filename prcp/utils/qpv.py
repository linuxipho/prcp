from pathlib import Path
import pandas as pd
import frappe

CODES_QP = dict(
    BA="BA - Quartier BEL AIR",
    EJ="EJ - Quartier EN JACCA (QP031016)",
    FE="FE - Quartier FENASSIERS",
    PO="PO - Quartier POITOU",
    VA="VA - Quartier VAL D'ARAN (QP031005)",
)

def run():

    file = 'data/BDD_2022_QPV.xlsx'
    path = Path(__file__).parent / file

    df = pd.read_excel(path, na_filter=False, index_col=None)

    df["Cause du chômage"].replace({
        "nc": 'NC',
        'licenciement': 'Licenciement',
        "Départ du pays d'origine ou déménagement": 'Déménagement'
    }, inplace=True)

    df["Publicité"].replace({
        "(non définie ou non communiquée)": 'NC'
    }, inplace=True)

    df["Civilité"].replace({
        'f': 'F',
        'm': 'M'
    }, inplace=True)

    df["DELD (oui/non)"].replace({
        'oui': 'Oui'
    }, inplace=True)

    df["Véhicule"].replace({
        'oui': 'Oui',
        'non': 'Non'
    }, inplace=True)

    df["Logement"].replace({
        'non': 'Non'
    }, inplace=True)

    df["Sortie positive"].replace({
        'NC': ''
    }, inplace=True)

    df['Pôle Emploi'].replace({
        "LA PLAINE": "La Plaine",
        "St- Michel": "Saint-Michel",
        "Toulouse St-michel": "Saint-Michel",
        "Occitane": "Toulouse Occitane",
        "St Georges": "Saint-Georges"
    }, inplace=True)

    df["Secteur d'activité"].replace({
        "Agen": "Agent",
        # "Agent d'accueil / Secretariat / Comptabilité": "",
        # "Agent d'entretien": None,
        "agent d'entretien": "Agent d'entretien",
        "Agent d'entretien  / Aide à domicile": "Agent d'entretien / Aide à domicile",
        # "Agent d'entretien / Aide à domicile": None,
        # "Association": "",
        # "Automobile": "",
        "autres": "Autre",
        "Banque/Assurance/Immobilier": "Banque / Assurance / Immobilier",
        # "BTP": "",
        # "Chef de projet": "",
        # "CIP": "",
        # "Commerce": "",
        # "Couture": "",
        # "Crèche": "",
        # "Créer son entreprise": "",
        # "Distribution Chronopost": "",
        # "Education": "",
        "Enseignante": "Education",
        "Gde distribution": "Grande distribution",
        "Gde distribution / vente": "Grande distribution",
        "Gde distribution / Vente": "Grande distribution",
        "Gde distribution/Vente": "Grande distribution",
        "Hotellerie / Restauration": "Hôtellerie / Restauration",
        # "Hôtellerie / Restauration": "",
        "Hotellerie/Restauration": "Hôtellerie / Restauration",
        # "Industrie": "",
        # "Informatique": "",
        "Ingeniorat": "Ingénierie",
        # "Jardinage": "",
        "Manutention/Cariste": "Manutention / Cariste",
        "Manutentionnaire/Cariste": "Manutention / Cariste",
        # "Paramédical / Médical": "",
        # "Parascolaire / Scolaire": "",
        # "Ressources Humaines": "",
        "Restauration": "Hôtellerie / Restauration",
        # "Sécurité": "",
        # "Social": "",
        # "Tourisme": "",
        # "Transport": ""
    }, inplace=True)

    df["Téléphone 1"].replace({
        '\\s+': ' '
    }, inplace=True, regex=True)

    df["Téléphone 2"].replace({
        '\\s+': ' '
    }, inplace=True, regex=True)

    df["Commentaire"] = df["Commentaire"].str.strip()
    df["Adresse électronique"] = df["Adresse électronique"].str.replace(',', '.')
    df["Adresse électronique"] = df["Adresse électronique"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')


    for idx, row in df.iterrows():

        try:
            code = CODES_QP[row['Code QP']]

            participant = frappe.db.get_list('Participant', filters={
                'nom_usuel': row['Nom'],
                'prenom': row['Prénom']
            })

            for p in participant:

                fiches_accueil = frappe.db.get_list('Accueil adherent', filters={
                    'participant': p.name
                })

                print(fiches_accueil)

                for name in fiches_accueil:

                    fiche = frappe.get_doc('Accueil adherent', name)
                    print(fiche, code)
                    fiche.set('code_qp', code)
                    print(fiche.get_valid_dict())
                    fiche.save()

        except frappe.ValidationError as e:
            print(row)
            raise e
