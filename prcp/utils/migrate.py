# import frappe
# import requests
# from pathlib import Path
#
# LOG = frappe.logger("prcp.utils", allow_site=True, file_count=10)
# BENCH_PATH = frappe.utils.get_bench_path()
#
#
# def update_translations():
#     """Update translation from online repo
#
#     Manually run with following command :
#     bench --site fabae.local execute fabae.utils.migrate.update_translations
#     """
#
#     apps = [
#         {
#             'source': 'https://gitlab.com/dokos/dodock/-/raw/develop/frappe/translations/fr.csv',
#             'target': 'apps/frappe/frappe/translations/fr.csv'
#         },
#         {
#             'source': 'https://gitlab.com/dokos/dokos/-/raw/develop/erpnext/translations/fr.csv',
#             'target': 'apps/erpnext/erpnext/translations/fr.csv'
#         }
#     ]
#
#     for app in apps:
#         r = requests.get(app.get('source'))
#         msg = f"Error while updating {app.get('target')}"
#         if r.status_code == 200:
#             fr = Path(BENCH_PATH, app.get('target'))
#             with fr.open('w') as f:
#                 f.write(r.text)
#             msg = f"{app.get('target')} successfully updated"
#         LOG.debug(msg)
#
#
# def fix_quotes_in_login_page(file):
#     """Fix quote type used to avoid error when translations with apostrophe"""
#
#     path = Path(BENCH_PATH, file)
#     text = path.read_text()
#     text = text.replace("'{{ _(\"", "\"{{ _('")
#     text = text.replace("\") }}'", "') }}\"")
#     path.write_text(text)
#
#
# def after_migrate():
#     """Post migration hooks"""
#
#     LOG.debug("Run post migration hooks")
#     update_translations()
#     fix_quotes_in_login_page('apps/frappe/frappe/templates/includes/login/login.js')
