import frappe

def run():

    recus = frappe.db.get_list('Recu', pluck='name')

    for recu in recus:

        doc = frappe.get_doc('Recu', recu)

        if not doc.email:
            continue

        file_name = f"PRCP-recu-fiscal-{doc.annee_fiscale}-{doc.denomination}"

        pdf = frappe.attach_print('Recu', doc.name, file_name=file_name, print_format='Reçu fiscal', doc=doc)
        sujet = 'Reçu fiscal PRCP - Point Rencontre Chômeurs et Précaires'
        msg = """<p style="padding-bottom:1em">Chère Madame, cher Monsieur,</p>
        <p style="padding-bottom:1em">L’Association PRCP - Point Rencontre Chômeurs et Précaires, a bien reçu le don que vous lui avez envoyé.</p>
        <p style="padding-bottom:1em">Votre soutien, expression de votre solidarité avec les chômeurs et précaires, donne à notre association une capacité d’innovation et de réactivité qui en améliore l’efficacité.</p>
        <p style="padding-bottom:1em">Votre don vous rend automatiquement adhérent de l'association PRCP et destinataire des informations sur ses réalisations.</p>
        <p style="padding-bottom:1em">Vous trouverez ci-joint, le reçu fiscal de votre don.</p>
        <p style="padding-bottom:2em">Au nom de l’ensemble des membres de l’association, je vous exprime mes plus vifs remerciements et vous adresse mes sincères salutations.</p>
        <p style="text-align: right">Le Vice-Président, Jacques Lavernhe</p>"""

        comment = frappe.get_doc(
            doctype="Communication",
            communication_medium='Email',
            communication_type='Communication',
            sent_or_received='Sent',
            # delivery_status='Sent',
            email_account='PRCP Colomiers',
            sender='admin@prcpcolomiers.fr',
            sender_full_name='PRCP Colomiers',
            recipients=doc.email,
            subject=sujet,
            content=msg,
            text_content=frappe.utils.strip_html_tags(msg),
            status='Linked',
            reference_doctype=doc.doctype,
            reference_name=doc.name
        )
        comment.insert()

        frappe.sendmail(
            recipients=doc.email,
            subject=sujet,
            message=msg, #frappe.render_template(dispatch_template.response, context),
            attachments=[pdf],
            # name=file_name,
            # header=("Reçu fiscal PRCP", "green"),
            with_container=1,
            add_unsubscribe_link=0,
            communication=comment.name,
            reference_doctype=doc.doctype,
            reference_name=doc.name
        )

        doc.submit()