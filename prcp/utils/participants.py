import frappe

from prcp.prcp.doctype.participant.participant import code_postal_dans_toulouse_metropole


def set_metropole():

    for p in frappe.db.get_list('Participant', fields=['name', 'code_postal']):
        print(p)
        cp = p.get('code_postal')
        if cp and code_postal_dans_toulouse_metropole(cp):
            print(cp)
            frappe.db.set_value('Participant', p.name, 'toulouse_metropole', 1)