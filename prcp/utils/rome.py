from pathlib import Path

import frappe
import pandas as pd

# LOG = frappe.logger("web", allow_site=True)


def create_node(base_doc):
    doc = frappe.get_doc(base_doc)
    doc.insert()
    return doc


def run():
    """Chargement du répertoir des métiers ROME"""

    file = 'data/ROME_ArboPrincipale.xlsx'
    path = Path(__file__).parent / file
    column_names = ['Famille', 'Domaine', 'Fiche', 'Libellé', 'Code OGR']
    df = pd.read_excel(path, sheet_name='Arbo Principale 14-06-2021', header=None, names=column_names, dtype='string',
                       na_filter=True, index_col=None, engine='openpyxl', skiprows=1)

    # On enlève les espaces qui trainent
    for col in column_names:
        df[col] = df[col].str.strip()

    # On supprime les lignes vides
    df.dropna(how='all', inplace=True)

    current_family = None
    current_domain = None
    current_form = None

    frappe.db.auto_commit_on_many_writes = 1

    root = frappe.get_doc(
        doctype='ROME', libelle='Répertoire des métiers', code='racine', is_group=True)
    root.insert()
    frappe.db.commit()

    for idx, row in df.iterrows():

        code: str = f"{row['Famille']}{row['Domaine']}{row['Fiche']}".strip()
        base_doc = dict(doctype='ROME', code=code, libelle=row['Libellé'], is_group=True)

        if not row['Fiche']:
            if not row['Domaine']:
                # Crée un nœud <famille>
                base_doc.update(dict(parent_rome=root.name))
                current_family = create_node(base_doc)
                print('Famille ->', current_family.name)
            else:
                # Crée un nœud <domaine>
                base_doc.update(dict(parent_rome=current_family.name))
                current_domain = create_node(base_doc)
        else:
            if not row['Code OGR']:
                # Crée un nœud <fiche>
                base_doc.update(dict(parent_rome=current_domain.name))
                current_form = create_node(base_doc)
            else:
                # Crée un nœud terminal <appellation métier>
                base_doc.update(dict(parent_rome=current_form.name, code_ogr=row['Code OGR'], is_group=False))
                create_node(base_doc)


if __name__ == '__main__':
    run()
