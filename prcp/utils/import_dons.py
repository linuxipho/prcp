from pathlib import Path

import frappe
import pandas as pd


def run():
    """Chargement du fichier des donateurs"""

    file = 'data/DONS_2022.xlsx'
    path = Path(__file__).parent / file
    df = pd.read_excel(path, na_filter=False, index_col=None, engine='openpyxl')

    # On itère sur chaque ligne du fichier
    for idx, row in df.iterrows():

        # On récupère et nettoie les données pour un accès plus rapide
        nom = row['NOM'].strip()
        prenom = row['PRENOM'].strip()
        conjoint = row['CONJOINT'].strip()
        moyen = row['MOYEN'].strip()
        # Si la ligne ne contient pas de nom, on passe à la ligne suivante
        if not nom:
            continue

        # On rassemble tous les dons de l'année dans une liste
        dons = [row[m] for m in range(1, 13) if row[m]]

        # S'il n'y a aucun don, on ignore la ligne et on passe à la suivante
        if not dons:
            continue

        # On récupère les participants existants à partir du nom de famille
        # TODO :: gérer les homonymes
        participant = frappe.db.get_all('Participant', filters={
            'nom_usuel': nom,
            'prenom': prenom
        }, pluck='name')

        if len(participant) > 1:
            print(participant)
            continue

        # Création du participant si non existant dans la base
        if not participant:

            print(f'Nouvel adhérent -> {nom} {prenom}')

            # On prépare quelques données
            cp = row['CP_VILLE'][:5] if row['CP_VILLE'] else None
            ville = row['CP_VILLE'][6:] if row['CP_VILLE'] else None
            tel = row['TEL'].strip() if row['TEL'] else None

            arrivee=frappe.utils.now_datetime() if pd.isnull(row['DEBUT']) else row['DEBUT']

            # On crée un dictionnaire contenant toutes les infos requises
            data = dict(
                doctype='Participant',
                nom_usuel=nom,
                prenom=prenom,
                adresse=row['ADRESSE'],
                code_postal=cp,
                commune=ville,
                tel=tel,
                arrivee=arrivee)

            # Insertion du nouveau participant dans la base de donnée
            doc = frappe.get_doc(data).insert()
            participant.append(doc.name)


        for mois in range(1, 13):
            valeur = row[mois]

            if not valeur:
                continue

            print(f"{type(valeur)} | {mois:02d} -> {valeur} €")

            frappe.get_doc(
                doctype='Don',
                participant=participant[0],
                date=f'2022-{mois:02d}-15',
                montant=valeur,
                moyen=moyen
            ).insert()


        # TODO :: gérer les conjoints manuellement


if __name__ == '__main__':
    run()
