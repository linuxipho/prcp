# Copyright (c) 2022, PRCP Colomiers and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname


class Sortieparticipant(Document):

	def autoname(self):
		year = self.get_value('date').year
		self.name = make_autoname(f"S{year}-.#####", '', doc=self)
