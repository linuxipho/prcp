# Copyright (c) 2023, PRCP Colomiers and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname


class Recu(Document):

	def autoname(self):
		# frappe.flags.in_import:
		annee = self.get_value('annee_fiscale')
		docname = f"RF{annee}-.#####"
		self.name = make_autoname(docname, '', doc=self)
