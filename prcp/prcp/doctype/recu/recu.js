// Copyright (c) 2023, PRCP Colomiers and contributors
// For license information, please see license.txt

frappe.ui.form.on('Recu', {
	refresh: function(frm) {

	},

    participant: function (frm) {
        frm.trigger('setAdresse');
    },

    setAdresse: function (frm) {
        frappe.db.get_doc('Participant', frm.doc['participant']).then(doc => {
            console.log(doc)
            // TODO remplir le champ adresse
        })
    }
});
