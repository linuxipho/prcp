# Copyright (c) 2022, PRCP Colomiers and contributors
# For license information, please see license.txt

import frappe
from frappe.utils.nestedset import NestedSet

import textwrap


class ROME(NestedSet):

	def autoname(self):
		if self.get('code') != 'racine':
			name = textwrap.shorten(f"{self.get('code')} - {self.get('libelle')}", width=140, placeholder="...")
		else:
			name = self.get('libelle')
		self.set('name', name)

# def on_doctype_update():
# 	print("ROME => on_doctype_update")
# 	frappe.db.add_index('ROME', ['lft', 'rgt'])
