// Copyright (c) 2021, PRCP Colomiers and contributors
// For license information, please see license.txt

frappe.ui.form.on('Accueil adherent', {

    /**
     * Methode appellée à chaque chargement de la page
     * @param frm   L'instance de formulaire
     */
    refresh: function (frm) {
        frm.page.set_title('Fiche accueil adhérent');
        // Ajout d'un bouton retour fiche participant
        frappe.utils.add_custom_button_with_class(frm, 'Aller à fiche participant', 'info', () => {
            frappe.set_route('Form', 'Participant', frm.doc['participant'])
        })
    },

    /**
     * Action déclenchée lorsqu'un participant est sélectionné
     * @param frm   L'instance de formulaire
     */
    participant: function (frm) {

        // On vérifie si la cotisation est bien réglée pour l'année en cours
        frappe.db.get_value('Cotisation', {
            participant: frm.doc['participant']
        }, 'paiement_complet').then(r => {
            // Si le paiement est complet, on coche la case correspondante
            if ('paiement_complet' in r.message)
                frm.set_value('cotisation_reglee', true);
        });
    }

});
