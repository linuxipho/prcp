# Copyright (c) 2022, PRCP Colomiers and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class Cotisation(Document):

	def validate(self):
		if self.get('paiements'):
			paiement = 0
			for p in self.get('paiements'):
				paiement += p.get('montant')
			if paiement >= 12:
				self.set('paiement_complet', 1)

