# Copyright (c) 2022, PRCP Colomiers and contributors
# For license information, please see license.txt
from datetime import date

import frappe
import requests
from frappe.model.document import Document
from frappe.model.naming import make_autoname


class Participant(Document):

	def autoname(self):
		# frappe.flags.in_import:
		year = self.get_value('arrivee').year or frappe.utils.now_datetime().year
		docname = f"P{year}-.###"
		self.name = make_autoname(docname, '', doc=self)

	def validate(self):
		# Définition du titre
		self.set('title', f"{self.get('nom_usuel')} {self.get('prenom')}")
		# Calcul de l'age
		self.compute_age()
		# Toulouse metropole
		if self.get_value('code_postal'):
			value = code_postal_dans_toulouse_metropole(self.get_value('code_postal'))
			if value is not None:
				self.set('toulouse_metropole', value)
		# Si pas de référence, on copie l'identifiant (name)
		if not self.get('ref'):
			print(self)
			self.set('ref', self.get('name'))
		# On crée la relation réciproque si un conjoint est défini
		if self.get_value('conjoint'):
			conjoint = frappe.db.get_value('Participant', self.get_value('conjoint'), 'conjoint')
			if self.get_value('conjoint') != conjoint:
				frappe.db.set_value('Participant', self.get_value('conjoint'), 'conjoint', self.name)

	def compute_age(self):
		if self.get_value('date_naissance'):
			today = date.today()
			birth = self.get_value('date_naissance')
			self.set('age', int((today - birth).days / 365.25))

def code_postal_dans_toulouse_metropole(cp='31770'):
	api = "https://geo.api.gouv.fr/communes"
	r = requests.get(api, params={'codePostal': cp})
	result = None
	if r.status_code == requests.codes.ok:
		response = r.json()
		if response:
			if len(response) > 1:
				result = None
			elif response[0]['codeEpci'] == '243100518':
				result = True
			else:
				result = False
	return result