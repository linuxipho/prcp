// Copyright (c) 2022, PRCP Colomiers and contributors
// For license information, please see license.txt

frappe.ui.form.on('Participant', {

    /**
     * Methode appellée à chaque chargement de la page
     * @param frm   L'instance de formulaire
     */
    refresh: function (frm) {

        // Si le formulaire est un nouvel enregistrement
        if (frm.is_new())
            frm.page.set_title('Nouveau participant');

        // Sinon on est sur un enregistrement existant
        else {

            // Si le flag de retour est actif, on retourne à la vue liste
            if (frappe.flags.return_to_list) {
                frappe.flags.return_to_list = false;
                setTimeout(() => frappe.set_route('List', 'Participant', 'List'), 300);
            }

            // Si le participant à une fiche accueil adhérent, on affiche un bouton de raccourcis
            frappe.db.get_list('Accueil adherent', {
                filters: {participant: frm.doc.name}, order_by: '-date_accueil', limit: 1, pluck: 'name'
            }).then(name => {
                if (name) {
                    frappe.utils.add_custom_button_with_class(frm, 'Fiche accueil adhérent', 'info', () => {
                        frappe.set_route('Form', 'Accueil adherent', name)
                    })
                }
            });
        }
    },

    /**
     * Methode appellée après l'enregistrement des données du formulaire
     * @param frm   L'instance de formulaire
     */
    after_save: function (frm) {
        // On définit un flag pour nous permettre de revenir à la vue liste
        frappe.flags.return_to_list = true;
    },

    /**
     * Action déclenchée lors de la saisie d'une date de naissance
     * @param frm   L'instance de formulaire
     */
	date_naissance: function (frm) {
        // Si une date est définie, on calcule l'age pour aujourd'hui
        if (frm.doc['date_naissance']) {
            let a = moment();
            let b = moment(frm.doc['date_naissance'], 'YYYY');
            let diff = a.diff(b, 'years');
            frm.set_value('age', diff);
        }
    }
});
