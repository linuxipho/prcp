// Copyright (c) 2022, PRCP Colomiers and contributors
// For license information, please see license.txt

frappe.ui.form.on('Promesse de don', {

	montant: function (frm) {
        setMontantAnnuel(frm)
    },

    frequence: function (frm) {
        setMontantAnnuel(frm);
    }
});

function setMontantAnnuel(frm) {
    let montant = frm.doc['montant'];
    if (montant && montant > 0)
        frm.set_value('montant_annuel', montant * facteurFrequence(frm.doc['frequence']));
    else
        frm.set_value('montant_annuel', 0);
}


function facteurFrequence(freq) {
    if (freq === 'Semestriel')
        return 2
    else if (freq === 'Trimestriel')
        return 4
    else if (freq === 'Mensuel')
        return 12
    else
        return 1
}