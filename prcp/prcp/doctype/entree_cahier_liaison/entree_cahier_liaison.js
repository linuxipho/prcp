// Copyright (c) 2022, PRCP Colomiers and contributors
// For license information, please see license.txt

frappe.ui.form.on('Entree cahier liaison', {
    refresh: function (frm) {
        if (frm.is_new())
            frm.page.set_title('Nouvelle entrée du cahier de liaison')
    },
    // participant: function (frm) {
    //     if (frm.doc['participant'])
    //         frm.call('get_participant_fullname').then(r => frm.refresh_field('nom_participant'));
    // }
})

