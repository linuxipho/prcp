# Copyright (c) 2022, PRCP Colomiers and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname


class Entreecahierliaison(Document):

	def autoname(self):
		year = self.get_value('date').year  # frappe.utils.now_datetime().year
		self.name = make_autoname(f"CL{year}-.#####", '', doc=self)

	# def validate(self):
	# 	self.get_participant_fullname()

	# @frappe.whitelist()
	# def get_participant_fullname(self):
	# 	prenom, nom = frappe.db.get_value('Participant', self.get('participant'), ['prenom', 'nom_usuel'])
	# 	self.set('nom_participant', f"{prenom} {nom}")
