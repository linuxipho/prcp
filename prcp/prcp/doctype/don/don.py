# Copyright (c) 2023, PRCP Colomiers and contributors
# For license information, please see license.txt
import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname

CIVILITE = dict(
	M='M. ',
	F='Mme ',
	NC=''
)


class Don(Document):

	def autoname(self):
		annee = self.get_value('date').year
		self.name = make_autoname(f"D{annee}-.#####", '', doc=self)


@frappe.whitelist()
def generation_des_recus_fiscaux(annee_fiscale):

	result = frappe.db.sql("""
		SELECT participant, SUM(montant) as 'total', GROUP_CONCAT(DISTINCT moyen) as 'moyens' FROM tabDon
		WHERE YEAR(date) = %(annee_fiscale)s
		GROUP BY participant
	""", values={'annee_fiscale': annee_fiscale}, as_dict=True, debug=True)

	print(result)

	for i in result:

		participant = frappe.get_doc('Participant', i.get('participant'))

		frappe.get_doc(
			doctype='Recu',
			annee_fiscale=annee_fiscale,
			participant=i.get('participant'),
			conjoint=participant.conjoint,
			denomination=get_denomination(participant),
			montant_total=i.get('total'),
			mode_versement=i.get('moyens')
		).insert()

def get_denomination(participant):
	return f"{CIVILITE[participant.genre]}{participant.nom_usuel} {participant.prenom}"