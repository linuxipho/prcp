frappe.listview_settings['Don'] = {

    // hide_name_column: true,

    onload: function (listview) {
        console.log(listview)
    },

    refresh: function (listview) {

        // Mise à jour du titre de la vue liste
        listview.page.set_title('Liste des dons');

        listview.page.add_inner_button('Générer les reçus fiscaux', () => {
            // frappe.warn('Voulez-vous continuer ?',
            //     `Cette action va générer le reçu fiscal pour chaque donnateur pour l'année fiscale <b>2022</b>. Assurez-vous que tous les dons de la période aient été renseignés.`,
            //     () => {
            //         // action to perform if Continue is selected
            //         frappe.call({
            //             method: 'prcp.prcp.doctype.don.don.generation_des_recus_fiscaux'
            //         })
            //     },
            //     'Continue',
            //     true // Sets dialog as minimizable
            // )

            let dialog = new frappe.ui.Dialog({
                title: 'Génération des reçus fiscaux',
                indicator: 'blue',
                fields: [{
                    label: "Cette action va générer le reçu fiscal de chaque donnateur pour ses dons effectués pendant l'année :",
                    fieldname: 'annee_fiscale',
                    fieldtype: 'Link',
                    options: 'Fiscal Year'
                }],
                primary_action_label: 'Continuer',
                primary_action(values) {
                    frappe.call({
                        method: 'prcp.prcp.doctype.don.don.generation_des_recus_fiscaux',
                        args: values
                    })
                    dialog.hide();
                }
            });

            dialog.show();


        })



        // // Enlever le filtre par nom
        // $('div[data-fieldname="title"]').remove();
    },
    //
    // formatters: {
    //     date(val) {
    //         // console.log(frappe.form.get_formatter('Date'));
    //         console.log(frappe.datetime.get_user_date_fmt().toUpperCase());
    //         return frappe.format(val, { fieldtype: 'Date' })
    //     }
    //
    // }
}


