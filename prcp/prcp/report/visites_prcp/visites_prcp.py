# Copyright (c) 2023, PRCP Colomiers and contributors
# For license information, please see license.txt

import frappe

DISTINCT = 'DISTINCT'
MONTH = 'MONTH'
QUARTER = 'QUARTER'
YEAR = 'YEAR'
ALL = ''

INTER = {
	'MONTH': {
		1: 'Janvier',
		2: 'Février',
		3: 'Mars',
		4: 'Avril',
		5: 'Mai',
		6: 'Juin',
		7: 'Juillet',
		8: 'Août',
		9: 'Sept.',
		10: 'Oct.',
		11: 'Nov.',
		12: 'Déc.',
		'total': 'Total'
	},
	'QUARTER': {
		1: 'T1',
		2: 'T2',
		3: 'T3',
		4: 'T4',
		'total': 'Total'
	}
}

LABEL = {
	DISTINCT: 'Visiteurs uniques',
	ALL: 'Nombre de visites'
}

def execute(filters=None):

	interval = QUARTER if filters.get('interval') == 'trimestre' else MONTH
	columns = get_columns(interval)

	result = []

	for request in [DISTINCT, ALL]:

		data = frappe.db.sql(get_query(interval, request), values=filters, as_dict=True)
		row = {'type': LABEL[request]} | {d.inter: d.count for d in data}

		total = frappe.db.sql(get_query(YEAR, request), values=filters, as_dict=True)
		if total:
			row['total'] = total[0].count

		result.append(row)

		print(result)

	return columns, result


def get_query(interval, distinct):
	return f"""SELECT {interval}(date) as inter, COUNT({distinct} participant) as count 
FROM `tabEntree cahier liaison` WHERE YEAR(date) = %(annee)s 
GROUP BY {interval}(date);
"""

def get_columns(interval):
	head = [{'fieldname': 'type', 'label': '', 'fieldtype': 'Data', 'width': 150}]
	cols = [{'fieldname': k, 'label': v, 'fieldtype': 'Int', 'width': 70} for k, v in INTER[interval].items()]
	return head + cols

