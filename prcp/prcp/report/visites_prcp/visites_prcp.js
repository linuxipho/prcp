// Copyright (c) 2023, PRCP Colomiers and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Visites PRCP"] = {

	onload: function (report) {
		report.page.set_indicator("Nombre de personnes inscrites fréquentant le PRCP", 'green');
	},

	filters: [
		{
			fieldname: "annee",
			label: "Année",
			fieldtype: "Select",
			default: new Date().getFullYear(),
			options: Array.from({ length: 11 }, (_, i) => new Date().getFullYear() - i)
		},
		{
			fieldname: "interval",
			label: "Interval",
			fieldtype: "Select",
			default: 'trimestre',
			options: [
				{value: 'trimestre', label: 'Par trimestre'},
				{value: 'mois', label: 'Par mois'}
			]
		}
	]
};
