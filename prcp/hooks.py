# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "prcp"
app_title = "PRCP"
app_publisher = "PRCP Colomiers"
app_description = "Point Rencontre Chômeurs Précaires"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "remi@chazelles.de"
app_license = "GPLv3"

fixtures = [
    {'dt': 'Role', 'filters': {'name': ['like', 'PRCP%']}},
    'Emploi',
    'ROME',
]

boot_session = "prcp.boot.boot_session"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "prcp.bundle.css"
app_include_js = "prcp.bundle.js"

# include js, css files in header of web template
# web_include_css = "/assets/prcp/css/prcp.css"
# web_include_js = "/assets/prcp/js/prcp.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "prcp/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#   "Role": "home_page"
# }

website_context = {
    "splash_image": "/assets/prcp/images/logo-prcp.png"
}

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "prcp.install.before_install"
# after_install = "prcp.install.after_install"
# after_migrate = "prcp.utils.migrate.after_migrate"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "prcp.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"Task": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

scheduler_events = {
    "daily": [
        "prcp.utils.tasks.daily"
    ]
}
# 	"all": [
# 		"prcp.tasks.all"
# 	],

# 	"hourly": [
# 		"prcp.tasks.hourly"
# 	],
# 	"weekly": [
# 		"prcp.tasks.weekly"
# 	]
# 	"monthly": [
# 		"prcp.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "prcp.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "prcp.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "prcp.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

