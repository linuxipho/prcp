// On désactive l'affichage de la sidebar
localStorage.show_sidebar = false;

// On désactive l'affichage en pleine largeur
localStorage.container_fullwidth = true;
frappe.ui.toolbar.toggle_full_width();

// Méthodes accessibles globalement
Object.assign(frappe.utils, {

    /**
     * Méthode
     * @param frm       L'instance de formulaire
     * @param title     Le titre du bouton
     * @param type      Le type de classe pour la couleur du bouton
     * @param action    L'action déclenché par le bouton
     */
    add_custom_button_with_class: function (frm, title, type, action, ) {
        frm.add_custom_button(title, action)
        frm.change_custom_button_type(title, null, 'info');
    }

})
